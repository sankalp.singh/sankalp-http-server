const fs = require("fs");
function readingFile(filename) {
	return new Promise((resolve, reject) => {
		if (typeof filename !== "string") {
			reject(Error("File path is not string"));
		} else {
			fs.readFile(filename, (err, data) => {
				if (err) {
					reject(err);
				} else {
					resolve(data);
				}
			});
		}
	});
}
module.exports = readingFile;
