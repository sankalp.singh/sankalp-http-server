const fs = require("fs");
const http = require("http");
const { v4: uuidv4 } = require("uuid");
const path = require("path");
const readingFile = require("./readingFile");
const port = 8080;

const server = http.createServer((req, res) => {
	const url = path.normalize(req.url);
	let reqUrl;
	if (url.endsWith("/")) {
		reqUrl = url.slice(0, -1);
	} else {
		reqUrl = url;
	}
	if (req.method !== "GET") {
		res.writeHead(405);
		res.write("Wrong Request Method");
		res.end();
	} else if (reqUrl === "/html") {
		readingFile("index.html")
			.then((data) => {
				res.writeHead(200, { "Content-Type": "text/html" });
				res.write(data);
			})
			.catch((err) => {
				console.error(err);
				res.writeHead(500);
				res.write("Failed to read file");
			});
	} else if (reqUrl === "/json") {
		readingFile("data.json")
			.then((data) => {
				res.writeHead(200, { "Content-Type": "application/json" });
				const jsonData = JSON.parse(data);
				res.write(JSON.stringify(jsonData));
				res.end();
			})
			.catch((err) => {
				console.error(err);
				res.writeHead(500);
				res.write("Failed to read the file");
				res.end();
			});
	} else if (reqUrl === "/uuid") {
		res.writeHead(200, { "Content-Type": "application/json" });
		const uniqueId = uuidv4();
		res.write(JSON.stringify({ uuid: uniqueId }));
		res.end();
	} else if (
		reqUrl.split("/")[1] === "status" &&
		reqUrl.split("/").length === 3
	) {
		if (reqUrl.split("/")[2] in http.STATUS_CODES) {
			res.writeHead(reqUrl.split("/")[2]);
			res.write(http.STATUS_CODES[reqUrl.split("/")[2]]);
			res.end();
		} else {
			res.writeHead(404);
			res.write("Wrong status code");
			res.end();
		}
	} else if (
		reqUrl.split("/")[1] === "delay" &&
		reqUrl.split("/").length === 3 &&
		reqUrl.split("/")[2] !== ""
	) {
		let delay = +reqUrl.split("/")[2];
		if (delay >= 0) {
			if (isNaN(delay)) {
				res.writeHead(404, { "Content-Type": "text/data" });
				res.write("Wrong URL!");
				res.end();
			} else {
				setTimeout(() => {
					res.writeHead(200, { "Content-Type": "text/data" });
					res.write("DELAY OF " + delay + " seconds");
					res.end();
				}, delay * 1000);
			}
		} else {
			res.writeHead(404);
			res.write("Wrong value!");
			res.end();
		}
	} else {
		res.writeHead(404, { "Content-Type": "text/data" });
		res.write("You have entered a wrong URL");
		res.end();
	}
});

server.listen(port, (err) => {
	if (err) {
		console.error(err.message);
	} else {
		console.log("Listening at port " + port);
	}
});
